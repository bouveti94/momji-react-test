import React, { useState, useEffect } from 'react';

import './App.css';

import Grid from './Components/Grid/Grid.jsx';

function App() {

  const [data, setData] = useState([]);

  //Data Fetching
  useEffect(() => {
    fetch('https://team.momji.fr/api/v2/static/employees').then(result => result.json()).then(data => setData(data));
  });

  //Flattening of received data for ag-Grid compatibility
  const parseData = (data) => {
    if(data.length === 0) return [];

    return data.map(row => {
      const result = {
        ...row,
        firstName: row.profile.firstName,
        lastName: row.profile.lastName
      };
      delete result.profile;

      return result;
    });
  };

  return (
    <div className="App">
      <Grid
        rowData={parseData(data)}
      />
    </div>
  );
}

export default App;
