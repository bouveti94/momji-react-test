import React, { useState, useCallback } from 'react';
import styled from 'styled-components';

const Modal = styled.div`
  position: absolute;
  background-color: rgba(255, 255, 255, 0.85);;
  width: 100%;
  height: 100%;
  top: 0px;
`;

const EditModal = (props) => {
  const {
    show,
    element
  } = props;

  const [value, setValue] = useState(element);

  //Edit value of a field
  const editField = useCallback((e) => {
    setValue({
      ...element,
      [e.target.name]: e.target.value
    });
  });

  //Submit new value. TODO: Send data to backend (currently in console.log)
  const submitEdit = useCallback((e) => {
    e.preventDefault();
    console.log(JSON.stringify(value));
  });

  //Build edit form from fields in selected element
  const editForm = (element) => {
    let result= [];
    for(const field in element) {
      result.push(
        <div>
          <label>{field}</label>
          <input
            name={field}
            type="text"
            placeholder={element[field].toString()}
            onChange={editField}
          />
        </div>
      );
    }
    return result;
  }

  if(!show) return null;

  return (
    <Modal>
      <form>
        {editForm(element)}
        <button onClick={submitEdit}>
          Submit
        </button>
        <button>
          Cancel
        </button>
      </form>
    </Modal>
  );
};

export default EditModal;
