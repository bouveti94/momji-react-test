import {AgGridColumn, AgGridReact} from 'ag-grid-react';

//Get columns name from data
export const getColumns = (data) => {
  const elem = data[0] || null;

  if(!elem) return [];
  return Object.keys(elem);
};

//Build ag-Grid columns
export const setupColumns = (columns) => {
  let result = [];
  for(const column of columns) {
    result.push(
      <AgGridColumn
        field={column}
        sortable={true}
        filter={true}
      />);
  }

  return result;
};
