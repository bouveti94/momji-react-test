import React, { useState, useRef } from 'react';
import {AgGridColumn, AgGridReact} from 'ag-grid-react';
import styled from 'styled-components';

import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';

import EditModal from "../EditModal/EditModal";
import {
  getColumns,
  setupColumns
} from "./utils.js"

const Grid = (props) => {
  const {
    rowData
  } = props;

  const [showEditModal, setShowEditModal] = useState(false);
  const [editRow, setEditRow] = useState(null);
  const gridRef = useRef(null);

  //Get selected row for edition and display edit modal
  const edit = (e) => {
    setEditRow(gridRef.current.api.getSelectedRows()[0]);
    setShowEditModal(true);
  };

  return (
    <div className="ag-theme-alpine Grid" style={{width: 1600, height: 900}}>
      <AgGridReact
        ref={gridRef}
        rowData={rowData}
        rowSelection='single'
        onSelectionChanged={edit}
      >
        {setupColumns(getColumns(rowData))}
      </AgGridReact>
     <EditModal
        show={showEditModal}
        element={editRow}
      />
    </div>
  );
};

export default Grid;
